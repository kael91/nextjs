import ItemCard from "../ItemCard"
export default function ListCard(props){
  console.log("ListCard Prop",props)
  return (<div>
    {props.data.map(e=>{
        return (<ItemCard key={e.id} data={e} />)
    })}
    </div>)
}
